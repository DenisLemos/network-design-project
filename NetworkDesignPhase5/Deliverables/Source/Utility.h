//
//  Utility.h
//
//
//  Created by Denis Lemos on February 1, 2014
//  Phase 1.
//

#ifndef Client_Utility_h
#define Client_Utility_h
#include "Includes.h"

#define COMMAND_UPLOAD					"upload"
#define COMMAND_DOWNLOAD				"download"
#define COMMAND_QUIT					"quit"
#define CONNECTION_HELLO 				"Server?"
#define CONNECTION_HELLO_ACK 			"I am here"

/**
 *  Types of command known in this custom protocol
 */
typedef enum {
    CommandTypeUpload,
    CommandTypeDownload,
    CommandTypeQuit,
    CommandTypeUnknown
}CommandType;

/**
 *  Validates server response in Connection Handshake
 *
 *  @param message server response to Connection attempt
 *
 *  @return TRUE if response is correct
 */
CBool validateServerAck(const char *message);

/**
 *  Checks if given command from client is the beginning of Connection handshake
 *
 *  @param message Message received from client
 *
 *  @return TRUE if message is beginning of connection handshake
 */
CBool isHelloMessage(const char *message);

/**
 *  Calculates checksum on given Packet data buffer and compares it with 
 *  Packet's checksum header
 *
 *  @param packetRef A Packet with data and a checksum
 *
 *  @return TRUE if checksum matches
 */
CBool isValidChecksum(Packet *packetRef);

/**
 *  Validates an ACK message
 *
 *  @param packRef A Packet with an ACK message on the first byte of the data buffer
 *
 *  @return TRUE if ACK message matches custom proocol's ACK
 */
CBool isAck(Packet *packRef);

/**
 *  Generates an 32 bit checksum from te data stored in the data buffer
 *
 *  @param data     buffer with data
 *  @param dataSize size of buffer
 *
 *  @return checksum created from data buffer.
 */
uint16_t makeChecksum(Byte *data, size_t dataSize);

/**
 *  Removes new line char from end of string and inserts null terminator in its place
 *
 *  @param string String to be examined and modified
 */
void removeNewLineChar(char *string);

/**
 *  Allocates a Packet and its buffer and returns a pointer to it (from heap)
 *  User must call free() on this object once done with it
 *  This function calls fill packet to make packet
 *
 *  @param data Data to be copied into Packet buffer, Set to NULL if you want an empty Packet
 *  @param size Size of data, Set to 0 if you want an empty Packet
 *  @param sequence of packet being sent
 *	@param type see PacketType
 *  @param fileSize Total size of file being sent over multiple packets
 *
 *  @return Pointer to a Packet object allocated in the heap
 */
Packet *makePacket(const void *data, size_t size, Byte sequence, PacketType type, size_t fileSize);

/**
 *  Sets all bytes in Packet->buffer to zero and data size to zero
 *
 *  @param packetRef Packet pointer
 */
void resetPacket(Packet *packetRef);

/**
 *  Copies data into Packet buffer and data size and generates its checksum
 *
 *  @param packet Packet to be modified
 *  @param data   Data to be copied into Packet->buffer
 *  @param size   Size of data
 *  @param sequence of packet being sent
 *	@param type see PacketType
 *  @param fileSize Total size of file being sent over multiple packets
 */
void fillPacket(Packet *packet, const void *data, size_t size, Byte sequence, PacketType type, size_t fileSize);


/**
 *  Uses command type in a simple switch statement with the command to call upload or download functions
 *
 *  @param this        this process connection info
 *  @param that        other process connection info
 *  @param commandType Command to inspect
 *  @param fileName    Filename to upload or download
 *
 *  @return Whether or not operation was successful
 */
CBool doCommad(Connection *this, Connection *that, CommandType commandType, char *fileName);

/**
 *  Inspects command string, checks if it is a known type and sets the CommandType pointer to the appropriate enum
 *
 *  @param command A string describing the command
 *  @param type    A pointer to a CommandType to be modified according to command string
 *
 *  @return TRUE if command string was known and CommandType was successfully set
 */
void validateCommand(const char *command, CommandType *type);

/**
 *  Uses strtok() to parse a space separated string for a command and an argument.
 *  This function restores command string upon successful parsing.
 *
 *  @param commandString Space separated input string with command and argument.
 *  @param command       Pointer to buffer in which the command token is to be written into.
 *  @param fileName      Pointer to buffer in which the argument token is to be written into.
 *
 *  @return TRUE if commandString was successfully parsed.
 */
CBool parseCommand(char *commandString, char *command, char *fileName);


#endif
