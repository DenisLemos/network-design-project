//
//  Networking.h
//
//
//  Created by Denis Lemos on February 17, 2014
//  Phase 2
//

#ifndef Client_Networking_h
#define Client_Networking_h
#include "Includes.h"

typedef void (*ProgressFunction)(float);

extern ProgressFunction sendProgress;
extern ProgressFunction receiveProgress;

/**
 *  Set the percentage of the time a given Testing Option Error should occur
 *
 *  @param percentage number from 0 to 50
 */
void setErrorProbability(float percentage);

/**
 *  Sets the Testing options according to ErrorOptions
 *
 *  @param option Which option to use in testing API (See Includes.h for each option description)
 */
void setTestingOption(ErrorOption option);


/**
 *  Uses simple string based protocol to check if there is a server visible at given IP address
 *
 *  @param this Client connection info
 *  @param that Server connection info
 *
 *  @return TRUE if server is found
 */
CBool checkConnection(Connection *this, Connection *that);

/**
 *  uses select() check if given Socket has anything to be read. This function blocks for timeToWait seconds.
 *
 *  @param socket     The Socket file description to be inspected.
 *  @param seconds    Time in seconds to block on Socket
 *  @param uSeconds	  Time in microseconds to block
 *
 *  @return TRUE if Socket has new data
 */
CBool anythingThere(Socket socket, int timeToWait, __darwin_suseconds_t uSeconds);


/**
 *  Sends a data in the message buffer of size size
 *
 *  @param this    this process connection info
 *  @param that    other process connection info
 *  @param message data buffer
 *  @param size    size of data buffer (must be smaller than KILOBYTE)
 *
 *  @return TRUE if data was sent successfully
 */
CBool rdt_send(Connection *this, Connection *that, const uint8_t *message, size_t size);

/**
 *  Receives data from that connection which gets written into the message buffer which 
 *  is allocated internally (if successful) and must be freed by caller.
 *
 *  @param this    this process connection info
 *  @param that    other process connection info
 *  @param message pointer to data buffer that will hold data received
 *  @param size    size of buffer allocated will be written into this pointer
 *
 *  @return TRUE if operations is successful
 */
CBool rdt_receive(Connection *this, Connection *that, uint8_t **message, size_t *size);



#endif
