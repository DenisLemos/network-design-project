//
//  Utility.c
//  Phase 1
//
//  Created by Denis Lemos on February 1, 2014
//
//

#include "Includes.h"
#include "DLNetworking.h"
#include "Utility.h"
#include <limits.h>


uint16_t  makeChecksum(Byte *data, size_t dataSize);
CBool readFile(char *fileName, Byte **destBuffer, size_t *bufferSize);
CBool saveFile(char *fileName, Byte *source, size_t bufferSize);

// ---------------------------------------------------------------------------
// ------------------------ Utility ------------------------------------------
// ---------------------------------------------------------------------------

Packet *makePacket(const void *data, size_t size, Byte sequence, PacketType type, size_t fileSize){
    
    Packet *pRef = (Packet *)calloc(sizeof(Packet), 1);
    
    // Empty packet
    if (data == NULL || size == 0) {
        pRef->dataSize = 0;
        pRef->sequence = 0;
        pRef->checksum = 0;
        pRef->type = PacketTypeUnknown;
		pRef->fileSize = 0;
        return pRef;
    }
    
    // Fill with data
    else{
		fillPacket(pRef, data, size, sequence, type, fileSize);
        return pRef;
    }
}

CBool isAck(Packet *packRef){
    return (Byte)packRef->buffer[0] == ACK && packRef->type == PacketTypeAck;
}

CBool isValidChecksum(Packet *packetRef){
    
    uint16_t thisChecksum = makeChecksum(packetRef->buffer, packetRef->dataSize);
    return thisChecksum == packetRef->checksum;
}

uint16_t makeChecksum(Byte *data, size_t dataSize){
    
    uint32_t checksum = 0;
    uint32_t temp = 0;
    
    if (dataSize % 2) {
        // Odd
        for (int i = 0; i < dataSize - 1; i += 2){
            temp = (data[i] << 8) & 0xFF00;
            temp |= data[i + 1];
            
            checksum += temp;
        }
        
        // Last one with padding
        temp = (data[dataSize - 1] << 8) & 0xFF00;
        checksum += temp  ;
        
    }else{
        // Even
        
        for (int i = 0; i < dataSize; i += 2){
            temp = (data[i] << 8) & 0xFF00;
            temp |= data[i + 1];
            
            checksum += temp;
        }
        
    }
    
    // Use uppper 16 bits to hold all carries and add them at the end to the sum
    checksum = (checksum >> 16) + (checksum & 0x0000FFFF);
    // Once more to get last carry
    checksum += (checksum >> 16) & 0x0000FFFF;
    
    return (uint16_t)~checksum;
}

void fillPacket(Packet *packet, const void *data, size_t size, Byte sequence, PacketType type, size_t fileSize){
    
    for (int i = 0; i < size; i++) {
        packet->buffer[i] = ((Byte *)data)[i];
    }
    
    packet->fileSize = fileSize;
    packet->dataSize = size;
    packet->type = type;
    packet->checksum = makeChecksum(packet->buffer, packet->dataSize);
    packet->sequence = sequence;
    
}

void resetPacket(Packet *packetRef){
    memset(packetRef, 0, KILOBYTE);
    packetRef->dataSize = 0;
    packetRef->sequence	= 0;
    packetRef->checksum	= 0;
    packetRef->type = PacketTypeUnknown;
    packetRef->fileSize = 0;
}

void removeNewLineChar(char *string){
	char *ptr = NULL;
	
	// Remove newline character from string
	if ( (ptr = strchr(string, '\n')) != NULL) {
		// Found newline: add null terminator there
		*ptr = '\0';
	}
}

void validateCommand(const char *command, CommandType *type){
    
	char compCommand[SMALL_BUFF];
    
    // -------------------- UPLOAD
	strcpy(compCommand, COMMAND_UPLOAD);
	if (strncasecmp(compCommand, command, strlen(compCommand)) == 0){
        *type = CommandTypeUpload;
        return;
    }
    
	// -------------------- DOWNLOAD
	strcpy(compCommand, COMMAND_DOWNLOAD);
	if (strncasecmp(compCommand, command, strlen(compCommand)) == 0){
        *type = CommandTypeDownload;
        return;
        
    }
    
    // -------------------- QUIT
	strcpy(compCommand, COMMAND_QUIT);
	if (strncasecmp(compCommand, command, strlen(compCommand)) == 0){
        *type = CommandTypeQuit;
        return;
        
    }
    
    // -------------------- Unknown
    else{
        *type = CommandTypeUnknown;
    }
}

CBool validateServerAck(const char *message){
	return (strncasecmp(CONNECTION_HELLO_ACK, message, strlen(message)) == 0);
}

CBool isHelloMessage(const char *message){
    return (strncasecmp(CONNECTION_HELLO, message, strlen(message)) == 0);
}

CBool parseCommand(char *commandString, char *command, char *fileName){
    
    char original[SMALL_BUFF];
    CBool success = FALSE;
    if (strlen(commandString) > KILOBYTE/2) {
        printf("\nERROR: Invalid commnad: %s", commandString);
        return FALSE;
    }
    strcpy(original, commandString);
    
    char *token;
    char *search = " ";
    
    // Parse Command
    success = ( (token = strtok((char *)commandString, search)) != NULL);
    if (success) {
        strcpy(command, token);
        
        // Parse Argument
        token = strtok(NULL, search);
        if (token == NULL) {
            success = (strncasecmp(COMMAND_QUIT, command, strlen(COMMAND_QUIT)) == 0);
            
        }
        else{
            strcpy(fileName, token);
            success = TRUE;
        }
    }
    strcpy(commandString, original);
    return success;
}

CBool doCommad(Connection *this, Connection *that, CommandType commandType, char *fileName){
    
    
    Byte *buffer;
    size_t bufferSize = 0;
    CBool success = FALSE;
    
    switch (commandType) {
        case CommandTypeDownload:
            
            printf("\nDownloading file [%s]", fileName);
            
            success = (rdt_receive(this, that, &buffer, &bufferSize) &&
                       saveFile(fileName, buffer, bufferSize));
         
            
            if (success && bufferSize > 0) free(buffer);
            
            return success;
            
            break;
            
            
        case CommandTypeUpload: {
            
            printf("\nReading file [%s]", fileName);
            readFile(fileName, &buffer, &bufferSize);
            if (bufferSize == 0) {
                printf("\n-------------------File not found");
            }

            success = rdt_send(this, that, buffer, bufferSize);
            
            if (success && bufferSize > 0 ) free(buffer);
            
            return success;
        }
            break;
            
        default:
            break;
            
    }
    
    // Should not get here
    return FALSE;
}

CBool saveFile(char *fileName, Byte *source, size_t bufferSize){

    FILE *filePointer;
    // Open file
    filePointer = fopen(fileName, "wb");
    
    if (filePointer == NULL) {
        printf("\nERROR:File not found");
        return FALSE;
    }
    
    else{
        
        // Write file
		assert(fwrite(source, 1, bufferSize, filePointer) == bufferSize);
        // Close file
        fclose(filePointer);
        return TRUE;
    }
    
}

CBool readFile(char *fileName, Byte **destBuffer, size_t *bufferSize){
    
    // Buffers
    Byte *buffer;
    FILE *stream;
    *bufferSize = 0;
    
    //Open the stream. Note "b" to avoid DOS/UNIX new line conversion.
    stream = fopen(fileName, "rb");
    
    if (stream != NULL) {
        
        //Steak to the end of the file to determine the file size
        fseek(stream, 0L, SEEK_END);
        *bufferSize = ftell(stream);
        fseek(stream, 0L, SEEK_SET);
        
        //Allocate enough memory (add 1 for the \0, since fread won't add it)
        buffer = malloc(*bufferSize * sizeof(Byte) + 1);
        
        //Read the file
        fread(buffer, *bufferSize, 1, stream);
        buffer[*bufferSize] = 0; // Add terminating zero.
        *destBuffer = buffer;
                
        // Close the file
        fclose(stream);
        
        return TRUE;
    }
    
    return FALSE;
}






