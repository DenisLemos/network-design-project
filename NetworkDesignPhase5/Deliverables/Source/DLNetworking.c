//
//  Networking.c
//  Phase 2
//
//  Created by Denis Lemos on February 1, 2014
//
//

#include "Includes.h"
#include "DLNetworking.h"
#include "Utility.h"

#define TIMER_TIMEOUT 1 // Time in seconds listener waits
#define RECEIVER_TIMEOUT 2 // In seconds
#define TRIAL_MAX 5
#define WINDOW_SIZE 4

ProgressFunction sendProgress = NULL;
ProgressFunction receiveProgress = NULL;

static ErrorOption		_testingOption = ErrorOption1; // default
static int _outOf = 20;

void setErrorProbability(float percentage){

    percentage =  (percentage >= 100)? 50 : percentage;
    
    int intPecentage= (int)percentage;
    if (intPecentage != 0) {
        _outOf = (1 / (float)intPecentage) * 100;
    }else _outOf = INT_MAX;
    
}

Packet *udt_receivePacket(Connection *this, Connection *that){

    Packet *packetRef = makePacket(NULL, 0, 0, PacketTypeUnknown, 0);  // Alloc
    
    ssize_t recvSize = 0;
    that->length = sizeof(that->address);
    
    recvSize = recvfrom(this->socket,
                        (Byte *)packetRef,
                        sizeof(Packet),
                        0,
                        (struct sockaddr*) &that->address,
                        (socklen_t *) &that->length);
    
    
    
    
    // -------------------- Fail
    if (recvSize == -1) {
        printf("\nERROR: recvfrom()");
        free(packetRef);
        return NULL;
        
    }
    
    // -------------------- Success
//    else if (packetRef->type != PacketTypeAbort){
//        printf("\n\n");
//        for (int i = 0; i < 20; i++) {
//            printf("%c", packetRef->buffer[i]);
//        }
//        
//        printf("\n\n");
//    }
    
    return  packetRef;
    
}

CBool udt_sendPacket(Connection *this, Connection *that, Packet *packetRef){
    
    
    assert(packetRef);
    
    if (_testingOption == ErrorOption5 && ((rand() % _outOf) == 1)) { // 1 out of 30 data packets will not be sent (lost)
        
        printf("\n\n\n---------------------- Dropping Data Packet\n\n");
        return TRUE;
        
    }
    
    
    printf("\n--------------Sending Packet %d", packetRef->sequence);
    Byte *wholePacketRef = (Byte *)packetRef;
    ssize_t sentSize = 0;
    if((sentSize = sendto(this->socket,
                          wholePacketRef,
                          sizeof(Packet),
                          0,
                          (struct sockaddr*)&(that->address),
                          that->length = sizeof(that->address))) == -1){
        printf("\nERROR: sendto()");
        return FALSE;
    }
    else{
        //        printf("\n\n%s\n\n", packetRef->buffer);
        CBool succes = sentSize == sizeof(Packet);
        if (!succes) {
            printf("\nERROR:Could not send entire packet");
        }
        
        return succes;
        
    }
    
    return FALSE;
}

CBool anythingThere(Socket socket, int seconds, __darwin_suseconds_t uSeconds){
	
	fd_set readMask;
	struct timeval timeout;
	CBool retVal;
	
	timeout.tv_sec = seconds;
	timeout.tv_usec = uSeconds;
	
	FD_ZERO(&readMask);
	FD_SET(socket, &readMask);
	
	if ( (retVal = select(32, &readMask, 0, 0, &timeout)) < 0 ) {  // Timer "Stops" if packet is received at this socket
		perror("select()");
        return FALSE;
	}
	return (FD_ISSET(socket, &readMask) && retVal);
	
}

size_t getNumberOfPackets(Byte *dataBuffer, ssize_t bufferSize){
    
    if (bufferSize == 0) {
        return bufferSize;
    }
    size_t numberOfPackets = bufferSize / KILOBYTE;
    numberOfPackets = (bufferSize == (int)(bufferSize / KILOBYTE ))? numberOfPackets : numberOfPackets + 1;
    return numberOfPackets;
}

Packet ** breakIntoPackets(Byte *dataBuffer, ssize_t bufferSize){
    if (bufferSize == 0 ) {
        return NULL;
    }
    
    size_t numberOfPackets = getNumberOfPackets(dataBuffer, bufferSize);
    Packet **packetBuffer = (Packet **)calloc(sizeof(Packet *), numberOfPackets);
    
    // Buffers
    size_t bytesCopied = 0;
    ssize_t nextChunkSize = 0;

    for (int i = 0; i < numberOfPackets; i++) {
        
        
        ssize_t remaining = bufferSize - bytesCopied;
        assert(remaining > 0);
        
        nextChunkSize = (remaining > KILOBYTE)? KILOBYTE : remaining;
        
        Packet *aPacket = makePacket(dataBuffer + bytesCopied,
                                     nextChunkSize,
                                     0, // Sequence number handled by caller
                                     PacketTypeData,
                                     bufferSize);
        
        bytesCopied += nextChunkSize;
        packetBuffer[i] = aPacket;
        
    }
    
    return packetBuffer;
}

typedef enum {
    ConnCheckHello,
    ConnCheckWait,
    ConnCheckFailed,
    ConnCheckSuccess
}ConnCheck;

CBool checkConnection(Connection *this, Connection *that){
    
    ConnCheck state = ConnCheckHello;
    char *hello = CONNECTION_HELLO;
    int trials = 1;
    static int kMaxTrials = 2;
    
    while (1) {
        switch (state) {
            case ConnCheckHello: printf("\nSending Connection Request");
                
                
                if (rdt_send(this, that, (uint8_t *)hello, strlen(hello)+ 1)) {
                    state = ConnCheckWait;
                    printf("\nHello sent");
                    
                }else{
                    state = ConnCheckFailed;
                    printf("\nERROR:Failed to send command");
                }
                
                break;
                
            case ConnCheckWait: printf("\nWaiting Connection Request");
                
                trials++;
                char *buffer;
                size_t size;
                
                if (rdt_receive (this, that, (uint8_t **)&buffer, &size)) {
                    
                    CBool ackCorrect = validateServerAck((const char *)buffer);
                    if (ackCorrect) {
                        state = ConnCheckSuccess;
                        
                    }else state = ConnCheckFailed;
                    
                }else{
                    if (trials >= kMaxTrials) {
                        state = ConnCheckFailed;
                    }
                    
                    else{
                        trials++;
                        state = ConnCheckHello;
                    }
                }
                
                break;
                
            case ConnCheckFailed: printf("\nConnection Failure");
                return FALSE;
                break;
                
            case ConnCheckSuccess: printf("\nConnection Success");
                free(buffer);
                
                return TRUE;
                break;
                
            default:
                break;
        }
    }
}

typedef enum {
    RDTReceive,
    RDTSend,
    RDTTimeout,
    RDTSendAck,
    RDTResendAck,
    RDTReceiveAck,
    RDTWait,
    RDTAbort,
    RDTExtractData,
    RDTAllocBuffer,
    RDTExitFailure,
    RDTExitSuccess,
}RDT_State;

static volatile CBool 		_timerValid;
static volatile CBool		_timerPending;
volatile static RDT_State 	_sendState;

void _timerFired(int arg){
    
    if (!_timerValid){
        _timerPending = TRUE;
        return;
    }
    
    printf("\n%s", __FUNCTION__);
    _sendState = RDTTimeout;
    
}

void _stopTimer(){
    
    _timerValid = FALSE;
    _timerPending = FALSE;
    printf("\n%s" , __FUNCTION__);
    
    struct itimerval time;
    
    time.it_interval.tv_sec = 0;
    time.it_interval.tv_usec = 0;
    time.it_value.tv_sec = 0;
    time.it_value.tv_usec = 0;
    setitimer(ITIMER_REAL, &time, 0);
    
    
}

void _startTimer(){
    
    printf("\n%s()" , __FUNCTION__);
    
    _timerPending = FALSE;
    _timerValid = TRUE;
    struct itimerval time;
    time.it_interval.tv_sec = 0;
    time.it_interval.tv_usec = 0;
    time.it_value.tv_sec = TIMER_TIMEOUT; /* 10 seconds timer */
    time.it_value.tv_usec = 0;
    
    signal(SIGALRM, _timerFired);
    setitimer(ITIMER_REAL, &time, 0);
    
}

CBool rdt_send(Connection *this, Connection *that, const uint8_t *message, size_t size){
    
	_sendState = RDTSend;
    int base = 0;
    unsigned nextSequence = 0;
    int timeoutCount = 0;

    size_t numberOfPackets = getNumberOfPackets((Byte *)message, size);
    
    printf("\nNumber of Packets to send = %lu", numberOfPackets);
    Packet **packetBuffer = breakIntoPackets((Byte *)message, size);
    
    while (TRUE) {
        
        switch (_sendState) { 
                
            case RDTWait: printf("\nRDTWait");
                // _sendState can also be changed by alarm interrupt which will end this loop
                while (_sendState == RDTWait) { // Wait for timer
                    
                    if (anythingThere(this->socket, 0, 100 )) { // OR ACK
                        _sendState = RDTReceiveAck;
                        break;
                        
                    }else if (nextSequence < numberOfPackets && (nextSequence < (base + WINDOW_SIZE)) ) {
                        _sendState = RDTSend;
                        break;
                    }
                    
                }
                break;
                
            case RDTSend: printf("\nRDTSend");
                
                timeoutCount = 0;
                printf("\nBASE: %d   NEXT SEQUENCE: %d", base, nextSequence);
                
                Packet *sndPkt;
                if (packetBuffer == NULL) {
                    sndPkt = makePacket(&sndPkt/*Just any pointer to void NULL code path*/,
                                        1,
                                        0,
                                        PacketTypeAbort,
                                        0);
                }
                else{
                    sndPkt = packetBuffer[nextSequence];
                }
                
                sndPkt->sequence = nextSequence;
                printf("\nSending With sequence %d", sndPkt->sequence);
                udt_sendPacket(this, that, sndPkt);
                if (base == nextSequence) {
                    _startTimer();
                }
                
                nextSequence++;
                _sendState = RDTWait;
                
                break;
                
            case RDTTimeout: printf("\nRDTTimeout");
                
                if (++timeoutCount == TRIAL_MAX ) {
                    _sendState = RDTExitFailure;
                    break;
                }printf("\n\nTimeout Count: %d", timeoutCount);
                
                _startTimer();
                for (int i = base ; i < nextSequence; i++) {
                    packetBuffer[i]->sequence = i;
                    udt_sendPacket(this, that, packetBuffer[i]);
                }
                _sendState = RDTWait;
                break;
                
            case RDTReceiveAck:
                
                _sendState = RDTWait;
                Packet *ackPacket =  udt_receivePacket(this, that);
                
                if (ackPacket != NULL) {
                    
                    
                    if (_testingOption == ErrorOption2 && (rand() % _outOf)  == 1 ) {

                            
                            uint16_t newAck = rand();
                            
                            printf("\n----------------------------------- Changing ACK from [%#04x] to [%#04x]",
                                   ackPacket->buffer[0],
                                   newAck);
                            
                            ackPacket->buffer[0] = newAck; // Sabotage Ack  to force a failure 1 out of 15 ACKS
                            

                    }
                    
                    // Testing Option 2 and 4 here
                    if (	((ackPacket->dataSize > 0) &&
                            isValidChecksum(ackPacket) &&
                            isAck(ackPacket))	) {
                        
                        
                        base = ackPacket->sequence + 1;
                        
                        if (sendProgress) {
                            sendProgress( base/ (float)numberOfPackets); // Progress callback
                        }

                        
                        if (base == nextSequence) {
                            _stopTimer();
                        }
                        else{
                            _startTimer();
                        }
                        
                        _sendState = (base < numberOfPackets) ? RDTWait : RDTExitSuccess ;
                        
                    }
                    
                     // -------------------- Ack Recieved Failure
                    else{
                        
                         // Test for bad ack data
                        if (isAck(ackPacket) == FALSE) {
                            
                            printf("(Ack failure)\n\
                                   Received 	[%#04x]\n\
                                   Should be	[%#04x]\n\n\n",
                                   ackPacket->buffer[0],
                                   ACK);
                        }
                        
                        _sendState = RDTWait;
                    }
                    // Detect Ack failure from option here
                    free(ackPacket);
                }
                
                break;
                
            case RDTExitSuccess:{ printf("\nRDTExitSuccess");
                
				for (int j = 0; j < numberOfPackets; j++) {
                    Packet *aPckt = packetBuffer[j];
                    free(aPckt);
                }
                free(packetBuffer);
                
                return base;
            }
                
                break;
                
                case RDTExitFailure: printf("\nRDTExitFailure");
                
                _stopTimer();
                
                for (int j = 0; j < numberOfPackets; j++) {
                    Packet *aPckt = packetBuffer[j];
                    free(aPckt);
                }
                free(packetBuffer);
                
                return FALSE;
                
                
                break;
                
                
            default:
                break;
        }
    }
    
    
    
    return FALSE; // Should never get herer
}

void setTestingOption(ErrorOption option){
    printf("\nSetting test option %d", option);
    srand((int)time(NULL));
    _testingOption = option;
}

Packet *ackPacketWithSequence(unsigned sequence){
    
    Byte data = ACK;
    Packet * ackPacket = makePacket(&data,
                           1,
                           sequence,
                           PacketTypeAck,
                           1);
    assert(ackPacket);
    return ackPacket;
    
}

CBool rdt_receive(Connection *this, Connection *that, Byte **message, size_t *size){
    
    size_t bytesCopied = 0;
    size_t expectedBytes = 0;
    unsigned expectedSeqNum = 0;
    int receiveTries = 0;
    
    Byte *tempBuffer = NULL;
    Packet *recPacket = NULL;
    Packet *ackPacket = ackPacketWithSequence(expectedSeqNum);

    RDT_State nextState = RDTReceive;

    
    while (TRUE) {
        
        switch (nextState) {

            case RDTWait: printf("\nRDTWait Receive");
                
                // Timeout in 10 seconds
                if (anythingThere(this->socket, RECEIVER_TIMEOUT, 0)) {
                    nextState = RDTReceive;
                    
                }
                else nextState = RDTResendAck;
                
                break;
                
            case RDTReceive: printf("\nRDTReceive");
                
                if (_testingOption == ErrorOption5 && (rand() % _outOf == 1)) {
                    printf("\n\n----------- Error Option 5: Drop received packet");
                    nextState = RDTWait;
                    break;
                }
                recPacket = udt_receivePacket(this, that);
                
                // ------------------------ Test 3 --------------------------------
                // Sabotage Data: 1 out of 5 packets will be sabotaged
                if (_testingOption == ErrorOption3 && (rand() % _outOf == 1)) {
                    printf("\n\n------- Error Option 3: Sabotage data integrity\n\n");
                    recPacket->buffer[rand() % recPacket->dataSize] = rand();
                }
                
                if (recPacket->type == PacketTypeAbort) {
                    nextState = RDTAbort;
                    break;
                }
                
                printf("\nReceived sequence %d  expected %d", recPacket->sequence, expectedSeqNum);

                
                 // -------------------- SUCCESS
                if ((recPacket != NULL &&
                    isValidChecksum(recPacket) &&
                    expectedSeqNum == recPacket->sequence)	) {
                    
                    expectedSeqNum++;
                    nextState = (bytesCopied == 0)? RDTAllocBuffer :RDTExtractData;
                    
                    if (receiveProgress) {
                        receiveProgress( (bytesCopied + recPacket->dataSize)/(float)recPacket->fileSize);
                    }
                    
                }else {
                 
                    nextState = RDTWait;
                    if (_testingOption == ErrorOption3) {
                        printf("\n\n\n---------------------- (Checksum failure)\n");
                    }
                    
                    else if (++receiveTries >= TRIAL_MAX) {
                        nextState = RDTExitFailure;
                        break;
                    }
                }
                
                break;
                
            case RDTAllocBuffer: printf("\nRDTAllocBuffer");
                
                if (recPacket->fileSize == 0) {
                    nextState = RDTExitFailure;
                }else {
                    tempBuffer = calloc(sizeof(Byte), recPacket->fileSize);
                    assert(tempBuffer);
                    expectedBytes = recPacket->fileSize;
                    nextState = RDTExtractData;
                }
                
                break;
                
            case RDTExtractData: printf("\nRDTExtractData");
                
                if (expectedBytes != recPacket->fileSize) {
                    nextState = RDTExitFailure;
                    break;
                }

                assert(tempBuffer);
                
                for (int j = 0; j < recPacket->dataSize ; j ++){
                    tempBuffer[bytesCopied + j] = recPacket->buffer[j];
                    
                }
              
                // Update offset
                bytesCopied += recPacket->dataSize;

                free(recPacket); recPacket = NULL;
                nextState = RDTSendAck;
                
                
//                // Show progress
//                float progress =((float)bytesCopied/(float)(*bufferSize)) * 100.0;
//                if (showProgress) showProgress(progress);
//                printf("\n------------------------------------------ Progress: %0.2f%%", progress);
//
                
                break;
                
            case RDTAbort: printf("\nRDTAbort");
                assert(ackPacket);
                
                udt_sendPacket(this, that, ackPacket);
                nextState = RDTExitSuccess;
                break;
        
            case RDTResendAck: printf("\nRDTResendAck");
             
                if (++receiveTries >= TRIAL_MAX) {
                    nextState = (bytesCopied == expectedBytes)?RDTExitSuccess: RDTExitFailure;
                    break;
                }printf("\nResend Ack Count %d", receiveTries);
                
                assert(ackPacket);
                udt_sendPacket(this, that, ackPacket);
                nextState = RDTWait;
                
                break;
                
                
            case RDTSendAck: printf("\nRDTSendAck");
               
                
                receiveTries = 0;
                assert(ackPacket);
    
                // Drop 5% of ack packets
                if (_testingOption == ErrorOption4 & (rand() % _outOf) == 1) {
                    printf("\n\n\n---------------------- Dropping ACK Package at sender");
                }
                else udt_sendPacket(this, that, ackPacket);
                ackPacket->sequence++;
                nextState = (bytesCopied == expectedBytes)?RDTExitSuccess : RDTWait;
                
                break;
                
            case RDTExitSuccess: printf("\nReceive Success");
                
                if (bytesCopied > 0) {
                    *message = &tempBuffer[0];
                }
                
                *size = bytesCopied;
                
                if (recPacket) free(recPacket);
                if (ackPacket) free(ackPacket);
                
                return TRUE;
                break;
                
            case RDTExitFailure: printf("\nReceive Failure");
                
                if (recPacket) free(recPacket);
                if (ackPacket) free(ackPacket);
                if (tempBuffer) free(tempBuffer);
                *message = NULL;
                return FALSE;
            
                break;
                
                
                
            default:
                break;
        }
    }
    
    return FALSE; // Should never get here
    
//    
//    CBool success = udt_receivePacket(this, that, packetRef);
//    
//    if (success) {
//        
//        // ------------------------ Test --------------------------------
//        // Sabotage data
//        if (_testingOption == ErrorOption3) {
//            for (int i = 0; i < packetRef->dataSize; i++) {
//                if ((rand() % 3000) == 1){ // 1 out of 3000 Bytes will be sabotaged
//                    packetRef->buffer[i] = rand();
//                }
//            }
//            
//        }
//        
//        // ------------------------ Test --------------------------------
//        if (!success && _testingOption == ErrorOption3) {
//            printf("\n\n\n---------------------- Option 3 Detected (Data bit error)\n\
//                   Received Checksum	[%#06x]\n\
//                   Should be			[%#06x]\n\n\n",
//                   packetRef->checksum,
//                   makeChecksum(packetRef->buffer, packetRef->dataSize));
//            
//        }
    
}




