//
//  Includes.h
//  Client
//
//  Created by Denis Lemos on February 1, 2014
//  Phase 1

#ifndef Client_Includes_h
#define Client_Includes_h

#include <stdio.h>

#include <sys/types.h>
#include <sys/socket.h>
#include <netdb.h>
#include <netinet/in.h>
#include <string.h>
#include <unistd.h>
#include <sys/fcntl.h>
#include <stdlib.h>
#include <stddef.h>
#include <dirent.h>
#include <sys/stat.h>
#include <sys/param.h>
#include <arpa/inet.h>
#include <pthread.h>
#include <errno.h>
#include <assert.h>
#include <signal.h>
#include <sys/time.h>


#ifdef __APPLE__
#include <malloc/malloc.h>

#elif __unix
#include <malloc.h>
#endif

// ------------------------ Global Constants --------------------------------
#define SMALL_BUFF					100
#define ACK						 	0x33

#define SERVER_PORT					8888
#define S_SERVER_PORT				"8888"

// ------------------------ Typedefs --------------------------------
// Custom Boolean type
typedef char CBool;
typedef uint8_t Byte;


#define TRUE 1
#define FALSE 0
#define KILOBYTE 1000

/**
 *  Used to distinguish Socket from an int
 */
typedef int Socket;

/**
 *  Used to pack Socket and sockaddr info
 */
typedef struct {
    Socket socket;
    struct sockaddr_in address;
    socklen_t length;
    
}Connection;

typedef enum {
    PacketTypeAck,
    PacketTypeData,
    PacketTypeAbort,
    PacketTypeUnknown
}PacketType;

typedef struct {
    size_t 			dataSize;
	size_t			fileSize;
    Byte 			buffer[KILOBYTE];
    uint16_t 		checksum;
    Byte 			sequence;
    PacketType 		type;
    
}Packet;

typedef enum {
    ErrorOption1 = 1, //  No loss/bit-errors
    ErrorOption2, //ACK packet bit-error: you need to intentionally change the data bits of the received ACK packet at the sender and implement suitable recovery mechanism.
    ErrorOption3, // Data packet bit-error: you need to intentionally change the data bits of the received DATA packet at the reveicer and implement suitable recovery mechanism.
    ErrorOption4,// ACK packet loss: you need to intentionally drop the received ACK packet at the sender and implement suitable recovery mechanism.
    ErrorOption5 //  Data packet loss: you need to intentionally drop the received DATA packet at the reveicer and implement suitable recovery mechanism.
    
}ErrorOption;


// ------------------------ Globals --------------------------------
extern int errno;						// Error code


#endif
