//
//  Server.c
//
//  Created by Denis Lemos on February 1, 2014
//
//	Phase 5
//

#include "Includes.h"
#include "DLNetworking.h"
#include "Utility.h"

// ------------------------ Constants --------------------------------
#define SERVER_TIMEOUT 1
#define ERROR_PROBABILITY 5.0 // Percent


// ------------------------ Function Declarations --------------------------------
CBool wasKeyPressed(void);
CBool processCommand(CommandType *command);
void enterLoop(Connection *serverRef, Connection *clientRef);

int main(int argc, const char * argv[]){
	
    Connection server, client;
    
	printf("\n\n");
	printf("----------------------------------------------------------------------------------------------------\n");
	printf("--------- Server Runs on an infinite loop. Press [ANY KEY] + [RETURN] to EXIT ----------------------\n");
    printf("--------- For Testing options, use argv[1] for Options 1, 2, 3, 4 or 5 (1 is default) --------------------\n");
	printf("----------------------------------------------------------------------------------------------------\n");
	
     // -------------------- Grab option from command prompt
    if (argc > 1) {
        ErrorOption argument = atoi(argv[1]);
        setTestingOption(argument);
    }
	
	// Create Datagram socket
    if ((server.socket = socket(AF_INET, SOCK_DGRAM, IPPROTO_UDP)) == -1){
        printf("\nERROR: socket()");
    }
    
    else printf("\nOpened Socket[%d]", server.socket);


    // Get reference to serverAddress structure
    struct sockaddr_in *serverAddRef = &server.address;
    
    // Create server address info
    bzero(serverAddRef, sizeof(struct sockaddr_in));
    serverAddRef->sin_family = AF_INET;
    serverAddRef->sin_port = htons(SERVER_PORT);
    serverAddRef->sin_addr.s_addr = htonl(INADDR_ANY);
    
    // Bind socket to address info
    if (bind(server.socket, (struct sockaddr* )serverAddRef, sizeof(struct sockaddr_in) )== -1){
        printf("\nERROR:bind()");
    }
    else {
        printf("\nBind successful");
    }

    setErrorProbability(0);
    
     // -------------------- Enter main loop
    enterLoop(&server, &client);
    
    close(client.socket);
    close(server.socket);
    
    return 0;
}

/**
 *  State Machine steps
 */
typedef enum {
    SSListen,
    SSCheckQuit,
    SSSendConnAck,
    SSParseMessage,
	SSExitSuccess,
    SSDoCommand,
    SSReset
}ServerState;


/**
 *  Server main loop which manages reading from stdin, this socket and performing incoming commands
 *
 *  @param serverRef this process connection info
 *  @param clientRef other process connection info
 */
void enterLoop(Connection *serverRef, Connection *clientRef){
    
    setErrorProbability(ERROR_PROBABILITY);
    
    // -------------------- Control
	ServerState state = SSListen;
    CommandType commandType;
    
    // -------------------- Buffers
    char command[SMALL_BUFF];
    char fileName[SMALL_BUFF];

    uint8_t *buffer = malloc(1);
    size_t bufferSize;
    
    
    while (1) {
		
		switch (state) {
                
			case SSListen: printf("\n.");
                
                if (anythingThere(serverRef->socket, SERVER_TIMEOUT, 0 )) {
					
                    printf("\nPacket Detected");
                    if (rdt_receive(serverRef, clientRef, &buffer, &bufferSize) ){
                        state = SSParseMessage;
                    }
                    else{
                        state = SSReset;
                    }
                    
                }
                else state = SSCheckQuit;
                break;
                
                
                
            case SSCheckQuit: printf("\n_");
                
                // -------------------- Poll if should continue
                CBool shouldQuit = wasKeyPressed();
                if (shouldQuit) {
                    printf("\nClosing Server\n");
                    state = SSExitSuccess;
                }
                else state = SSListen;
                break;
                
                
            case SSParseMessage: printf("\nSSParseMessage");
                
                // -------------------- HELLO
                if (isHelloMessage((char *)buffer)) {
                    state = SSSendConnAck;
                }
                
                // -------------------- COMMAND
                else{
                    if (parseCommand((char *)buffer, command, fileName)){
                        validateCommand(command, &commandType);
                    }else{
                        printf("\nERROR:Could not parse command [%s]", (char *)buffer);
                        commandType = CommandTypeUnknown;
                    }
                    
                    switch (commandType) {
                            
                        case CommandTypeQuit:
                            printf("\nCommand Received: [quit]");
                            state = SSExitSuccess;
                            break;
                            
                        case CommandTypeUnknown:
                            printf("\nERROR:Invalid Command Received");
                            state = SSReset;
                            break;
                            
                        default:
                            printf("\nValid Command Received");
                            state = SSDoCommand;
                            break;
                    }
                }
                
                break;
                
                
            case SSSendConnAck: {
                
                char *helloAck = CONNECTION_HELLO_ACK;
                printf("\nSending server connection ack");
                rdt_send(serverRef, clientRef, (uint8_t *)helloAck, strlen(helloAck)+1);
                
                state = SSReset;
                
            }
                break;
                
                
            case SSDoCommand:
                
                if (processCommand(&commandType)) {
                    
                    if (doCommad(serverRef, clientRef, commandType, fileName)) {
                        printf("\nCommand Executed");
                    }
                    
                    else{
                        printf("\nERROR:Failed to execute command");
                    }
                }
                
                state = SSReset;
                break;
                
			case SSExitSuccess: printf("\nS-> ");
                
				return; // Exits main loop here
				break;
				
                
            case SSReset: printf("\nSSReset");
                
                bzero(command, SMALL_BUFF);
                bzero(fileName, SMALL_BUFF);
				commandType = CommandTypeUnknown;
                state = SSListen;
                
                break;
				
			default:
				break;
                
		}
	}
    
}

/**
 *  This Function takes in the command type received and tests if it is known command
 *  if the command is know it converts its meaning from the the perspective of the client to
 *  the perspective of the server.
 *
 *  @param command Pointer to command received from Client
 *
 *  @return TRUE if command is valid
 */
CBool processCommand(CommandType *command){
    
    switch (*command ) {
        case CommandTypeUpload:
            *command = CommandTypeDownload;
            return TRUE;
            break;
            
        case CommandTypeDownload:
            *command = CommandTypeUpload;
            return TRUE;
            
        default:
            return FALSE;
            break;
    }
    
    return FALSE;
}

/**
 *
 *
 *  @param void Uses select() to inspect the standard input for any input.
 *
 *  @return TRUE if any key was pressed
 */
CBool wasKeyPressed(void){
	
	fd_set readMask;
	CBool retVal;
	struct timeval timeout;
	
	timeout.tv_sec = 0;
	timeout.tv_usec = 0;
	
	FD_ZERO(&readMask);
	FD_SET(STDIN_FILENO, &readMask);
	
	if ( (retVal = select(1, &readMask, 0, 0, &timeout)) < 0 ) {
		perror("ERROR:select() in wasKeyPressed");
	}
	return FD_ISSET(STDIN_FILENO, &readMask);
	
}

