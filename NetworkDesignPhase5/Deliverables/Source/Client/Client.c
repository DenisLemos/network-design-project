//
//  Client.c
//  Client
//
//  Created by Denis Lemos on February 1, 2014.
//  Phase 5
//

#include "Includes.h"
#include "Utility.h"
#include "DLNetworking.h"

#define ERROR_PROBABILITY 5.0 // Percent

void enterMainLoop(Connection *serverRef, Connection *clientRef);

int main(int argc, const char * argv[]){
	
    Connection client, server;
    
    if(argc < 2){
        printf("\nERROR: Usage : %s <Server-IP> <Option>\n",argv[0]);
        exit(EXIT_FAILURE);
    }
    
    printf("\n\n");
	printf("----------------------------------------------------------------------------------------------------\n");
    printf("--------- For Testing options, use argv[2] for Options 1, 2, 3, 4, 5 (1 is default) --------------------\n");
	printf("----------------------------------------------------------------------------------------------------\n");
    // -------------------- Grab option from command prompt
    if (argc > 2) {
        ErrorOption argument = atoi(argv[2]);
        setTestingOption(argument);
    }
    
    // Create Datagram socket
    if ((client.socket = socket(AF_INET, SOCK_DGRAM, IPPROTO_UDP))==-1){
        printf("\nERROR:socket()");
        return EXIT_FAILURE;
    }
    
    printf("\nClient opened Socket[%d]", client.socket);


    // Create server address info
    bzero(&server.address, sizeof(server.address));
    server.address.sin_family = AF_INET;
    server.address.sin_port = htons(SERVER_PORT);
    server.address.sin_addr.s_addr = inet_addr(argv[1]);
    
    
    printf("\nServer Address[%s] Port[%d]",
           argv[1], ntohs(server.address.sin_port));
    
    setErrorProbability(0);
    if (checkConnection(&client, &server)) {
        // -------------------- Enter Main Loop here
        setErrorProbability(ERROR_PROBABILITY);
        enterMainLoop(&server, &client);
    }

    close(client.socket);
    close(server.socket);
    
    return 0;
}

/**
 *  Typedf enum used to control Client state machine
 */
typedef enum {
    CSStdin,
    CSReset,
    CSSendCommand,
    CSWaitForAck,
    CSDoCommand,
    CSExitSuccess
}ClientState;

/**
 *  Main loop that handles reading stdin and network functions
 *
 *  @param serverRef server information
 *  @param clientRef client information
 */
void enterMainLoop(Connection *serverRef, Connection *clientRef){

    
     // -------------------- Control
    CommandType commandType;
    
    
     // -------------------- Buffers
    char stringBuffer[SMALL_BUFF]; // Command string
    char command[SMALL_BUFF];
    char fileName[SMALL_BUFF];
    
    
    ClientState state = CSStdin;
	
    while (TRUE) {
        
        switch (state) {
            case CSReset: printf("\nRESET");
                
                bzero(stringBuffer, SMALL_BUFF);
                bzero(command , SMALL_BUFF);
                bzero(fileName, SMALL_BUFF);
                commandType = CommandTypeUnknown;
                state = CSStdin;
                
                break;
                
            case CSStdin: printf("\n.");
                
                // -------------------- Prompt and read command
                printf("\nEnter Command [upload *], [download *], [quit]: ");
                fgets(stringBuffer, SMALL_BUFF, stdin);
                removeNewLineChar(stringBuffer);
                
                if(parseCommand(stringBuffer, command, fileName)){
                    validateCommand(command, &commandType);
                }else{
                    printf("\nERROR:Could not parse command [%s]", stringBuffer);
                    commandType = CommandTypeUnknown;
                }
                
                switch (commandType) {
                        
                    case CommandTypeQuit:
                        printf("\nCommand Entered: [quit]");
                        state = CSExitSuccess;
                        break;
                        
                    case CommandTypeUnknown:
                        printf("\nERROR:Invalid Commnand Entered");
                        state = CSReset;
                        break;
                        
                    default:
                        state = CSSendCommand;
                        printf("\nCommand Entered: [%s] with filename: [%s]",  command, fileName);
                        break;
                }
  
                break;
                
            case CSSendCommand: printf("\n..");
                
                if (rdt_send(clientRef, serverRef, (uint8_t *)stringBuffer, strlen(stringBuffer) + 1) ) {
                    state = CSDoCommand;
                    printf("\nMessage sent");
                    
                }else{
                    state = CSReset;
                    printf("\nERROR:Failed to send command");
                }
                
                break;
            
                
            case CSDoCommand: printf("\n....");
                
                
                if (doCommad(clientRef, serverRef, commandType, fileName)) {
                    printf("\nCommand Executed");

                }
                else{
                    printf("\nERROR:Failed to execute command");

                }
                
                
                state = CSReset;
                
                break;
                
            case CSExitSuccess:
        
                printf("\nExiting\n");

                return; // Exits Main loop here
                
                break;
                
            default:
                break;
        }
    }

}