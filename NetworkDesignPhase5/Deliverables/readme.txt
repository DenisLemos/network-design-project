

Note that the directions bellow assume that the submitted directory is /Deliverables. 
HOWEVER I am submiting the my entire project structure so tha you have access to the iOS project. 
I am only doing this so you can se the Objctive C code. which is at 

/iPhoneApp/... 

from then on you can tap on ECE 483-201.xcworkspace to open the project. But you can only view this project on a Mac.
But even without a Mac you can also view the Ojbective-C code source files by navigating to /iPhoneApp/ECE \483-201/ECE \483-201/
Here you will see the .h and .m files I have crated to work with the C API for this project.
If you wish to run the iOS app you can send me an email and I can send you a build. (if you have an iOS app yourself)

If you only wish to view the C code which satisfies all requirements of this project 
you can navigate to 

/NetworkDesignPhase5/Deliverables/

In this directory you will find the following 

________________________________________
The following items are being submitted  

- readme.txt			(This file)

- Source PDF/   		(Directory with PDF versions of source code)
	
	- SharedSource.pdf  (PDF versions of Includes, Utility and DLNetworking)
	- Server.pdf  		(PDF version of Server.c)
	- Client.pdf 		(PDF version of Client.c)

- DesignDocument.pdf 	(Describes the purpose of each module)

- Charts/				(Phase 5 charts)
	
	- Phase5Charts.pdf  	(PDF export of chart, which is the optimal format for viewing)
	- Phase5Charts.numbers 	(Native file where document was created, only works on Macs)
	- Phase5Charts.xlsx		(Excel export of native file, which kinda works but it is still gross to look at)

- Source/				(Directory contains source code and makefiles used in this project)

	- Includes.h			(Includes useful .h files and defines useful
							 data structures and constants used throughout)

	- Utility.h/c			(Provides string manipulation, command 
							validation, and Packet manipulation)

	- DLNetworking.h/c      (Provides udt and rdt send/receive and rdt upload/download)

	- Client/				(Directory contains source, make and text 
							files pertinent to the Client program)

			- Client.c			(Client source code)
			- makefile			(Compiles and links Client and helper files at
								directory at ./.. and ./) * Directory structure must not be modified.*

	- Server/			(Directory contains source, make and text 
						files pertinent to the Server program)

			- Server.c			(Server source code)
			- makefile			(Compiles and links Server and the helper files at the
								directory at ./.. and ./) * Directory structure must not be modified. *
			- Sample Files



________________________________________
System requirements:

Any Unix based OS such as Ubuntu or Mac OS (Not tested on Windows)

________________________________________
To run Server app:

0. (Optional) Add any file of any type and size into this directory to upload/download.
1. navigate to 'Server' Directory
2. type 'make' to execute makefile and build executable
3. type './Server' or './Server 1' or './Server 2' or './Server 3'  (or 4 or 5) to run Server app. 
5. Press any key + Return to terminate Server session.
________________________________________
To run Client app:

0. (Optional) Add any file of any type and size into this directory to upload/download.
1. navigate to 'Client' Directory
2. type 'make' to execute makefile and build executable
3. type './Client <Server IP Address here> <test option here>'
	e.g. ./Client 192.168.1.21 3 <return>


	Sample Commands:
	$ upload Client.c
	$ download Server.c
	$ download TestImage.jpg
	# download Server
	$ quit

Then check file contents. You can also upload/download any other file present inside thie directory, including source files, and executables.

