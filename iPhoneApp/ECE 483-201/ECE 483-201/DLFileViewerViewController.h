//
//  DLFileViewerViewController.h
//  ECE 483-201
//
//  Created by Denis Lemos on 3/9/14.
//  Copyright (c) 2014 UMASS Lowell. All rights reserved.
//

#import <UIKit/UIKit.h>

@class DLFile;
@interface DLFileViewerViewController : UIViewController
- (id)initWithFile:(DLFile *)file;
@end
