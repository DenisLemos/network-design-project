//
//  DLNetworkingBridge.h
//  ECE 483-201
//
//  Created by Denis Lemos on 3/8/14.
//  Copyright (c) 2014 Denis Lemos. All rights reserved.
//

#import <Foundation/Foundation.h>
#include "DLNetworking.h"

@interface DLNetworkingBridge : NSObject

+ (BOOL)isConnectd;
+ (void)connectWithIPString:(NSString *)ipString completionBlock:(void (^)(BOOL success))completion;
+ (void)setErrorProbability:(CGFloat)percentage;
+ (void)uploadData:(NSData *)data
          fileName:(NSString *)name
   completionBlock:(void (^)(BOOL success))completion;

+ (void)downloadFileName:(NSString *)name
   completionBlock:(void (^)(BOOL success, NSData *fileData))completion;

@end
