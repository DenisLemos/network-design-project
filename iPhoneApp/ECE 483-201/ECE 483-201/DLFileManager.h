//
//  DLFileManager.h
//  ECE 483-201
//
//  Created by Denis Lemos on 3/6/14.
//  Copyright (c) 2014 Denis Lemos. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "DLFile.h"

@interface DLFileManager : NSObject
- (void)loadFiles;
- (NSInteger)numberOfFiles;
- (DLFile *)fileObjectAtIndex:(NSInteger )index;
- (NSInteger)indexOfFile:(DLFile *)file;
- (DLFile *)fileWithName:(NSString *)fileNameWithExtension;
- (void)addFileWithName:(NSString *)fileName data:(NSData *)data;
- (void)deleteFileAtIndex:(NSInteger)index;
@end
