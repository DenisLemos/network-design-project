//
//  main.m
//  ECE 483-201
//
//  Created by Denis Lemos on 3/6/14.
//  Copyright (c) 2014 Denis Lemos. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "DLAppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([DLAppDelegate class]));
    }
}
