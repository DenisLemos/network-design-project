//
//  DLCoreDataStack.h
//  ECE 483-201
//
//  Created by Denis Lemos on 3/6/14.
//  Copyright (c) 2014 Denis Lemos. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface DLCoreDataStack : NSObject
+ (void)saveContext;

+ (NSManagedObjectContext *)managedObjectContext;

@property (strong, nonatomic) NSManagedObjectContext *managedObjectContext;
@property (strong, nonatomic) NSManagedObjectModel *managedObjectModel;
@property (strong, nonatomic) NSPersistentStoreCoordinator *persistentStoreCoordinator;

@end
