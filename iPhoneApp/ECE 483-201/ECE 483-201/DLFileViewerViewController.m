//
//  DLFileViewerViewController.m
//  ECE 483-201
//
//  Created by Denis Lemos on 3/9/14.
//  Copyright (c) 2014 UMASS Lowell. All rights reserved.
//

#import "DLFileViewerViewController.h"
#import "DLFile.h"
@interface DLFileViewerViewController ()
@property (nonatomic, strong) DLFile *file;


@end

@implementation DLFileViewerViewController

- (id)initWithFile:(DLFile *)file{
    
    if (self =  [super initWithNibName:nil bundle:nil]) {
        self.file = file;
    }
    return self;
}
- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.navigationItem.title = [self.file fullName];
    self.edgesForExtendedLayout	= UIRectEdgeNone;
    CGRect contentFrame = self.view.frame;
    contentFrame.size.height -= self.navigationController.navigationBar.frame.size.height + [[UIApplication sharedApplication]statusBarFrame].size.height;
    
    UIImage *image = [UIImage imageWithData:self.file.fileData scale:1];
    
    if (image) {
         // -------------------- File is image
        UIImageView *imageView = [[UIImageView alloc]initWithImage:image];
        imageView.backgroundColor = [UIColor blackColor];
        imageView.frame = self.view.frame;
        imageView.contentMode = UIViewContentModeScaleAspectFit;
        [self.view addSubview:imageView];
        
    }
    
    
     // -------------------- Assume it is some sort of text file
    else{
        

        UITextView *textView = [[UITextView alloc]initWithFrame:contentFrame];
        
        textView.editable = NO;
        textView.scrollEnabled = YES;

        char *cStringBuffer = malloc([self.file.fileData length]);
        [_file.fileData getBytes:&cStringBuffer[0] length:[self.file.fileData length]];
        assert(cStringBuffer);
        NSString *string = [NSString stringWithFormat:@"%s", cStringBuffer];
        assert(string);
        textView.text = string;
        [self.view addSubview:textView];
        
        free(cStringBuffer);
        

    }
    
    
    

}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


@end
