//
//  DLFileManager.m
//  ECE 483-201
//
//  Created by Denis Lemos on 3/6/14.
//  Copyright (c) 2014 Denis Lemos. All rights reserved.
//

#import "DLFileManager.h"
#import "DLCoreDataStack.h"


@interface DLFileManager (){
    NSArray *_fileObjects;
}

@end

@implementation DLFileManager

#pragma mark - Public
- (void)loadFiles{
    
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc]init];
    NSEntityDescription *entity = [NSEntityDescription entityForName:[DLFile entityName]
                                              inManagedObjectContext:[DLCoreDataStack managedObjectContext]];
    
    [fetchRequest setEntity:entity];
    
    NSError *error = nil;
    NSArray *fetchedRequests = [[DLCoreDataStack managedObjectContext]executeFetchRequest:fetchRequest
                                                                         error:&error];
    
    if (error) {
        NSLog(@"Error %@", error);
        _fileObjects = nil;
        return;
    }
    
    _fileObjects = fetchedRequests;
}

- (NSInteger)numberOfFiles{
    return [_fileObjects count];
}

- (NSArray *)fileObjects{
    if (_fileObjects == nil || [_fileObjects count]== 0 ) {
        [self loadFiles];
    }
    return _fileObjects;
}

- (DLFile *)fileObjectAtIndex:(NSInteger )index{
    return [[self fileObjects]objectAtIndex:index];
}
- (NSInteger)indexOfFile:(DLFile *)file{
    return [[self fileObjects]indexOfObject:file];
}

- (DLFile *)fileWithName:(NSString *)fileNameWithExtension{

    for (DLFile *file in [self fileObjects]) {
      
        if ([fileNameWithExtension isEqualToString:[file fullName]]) {
            return file;
            
        }
    }
    
    return nil;;
}

- (void)addFileWithName:(NSString *)fileName data:(NSData *)data{
    
    DLFile *fileEntityDescription = [NSEntityDescription insertNewObjectForEntityForName:[DLFile entityName]
                                                                inManagedObjectContext:[DLCoreDataStack managedObjectContext]];
    
    assert(fileEntityDescription);
    
    fileEntityDescription.fileData = data;
    fileEntityDescription.timestamp = [NSDate date];
    fileEntityDescription.fileName = [fileName stringByDeletingPathExtension];
    fileEntityDescription.extension = [fileName pathExtension];
    
    [DLCoreDataStack saveContext];
    [self loadFiles];
    
}


- (void)deleteFileAtIndex:(NSInteger)index{
    
    DLFile *deletedFile = [self fileObjectAtIndex:index];
    [[DLCoreDataStack managedObjectContext]deleteObject:deletedFile];
    [DLCoreDataStack saveContext];
    [self loadFiles];

}

@end
