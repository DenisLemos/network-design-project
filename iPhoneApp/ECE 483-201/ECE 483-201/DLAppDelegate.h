//
//  DLAppDelegate.h
//  ECE 483-201
//
//  Created by Denis Lemos on 3/6/14.
//  Copyright (c) 2014 Denis Lemos. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface DLAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


- (DLAppDelegate *)delegate;

@end
