//
//  DLMainViewController.m
//  ECE 483-201
//
//  Created by Denis Lemos on 3/6/14.
//  Copyright (c) 2014 Denis Lemos. All rights reserved.
//

#import "DLMainViewController.h"
#import "DLFileManager.h"
#import "UIAlertView+Blocks.h"
#import "DLNetworkingBridge.h"
#import "DLFile.h"
#import "DLFileViewerViewController.h"


//#define AUTO_FILL
static NSString *kIpKey = @"savedServerIP";

@interface DLMainViewController ()<UITableViewDataSource, UITableViewDelegate, UITextFieldDelegate>{
    DLFileManager *_fileManager;
    DLFile *_selectedFile;
}

@property (weak, nonatomic) IBOutlet UITextField *fileTextField;
@property (weak, nonatomic) IBOutlet UIButton *serverIPButton;
@property (nonatomic, weak) IBOutlet UITableView *tableView;
@property (weak, nonatomic) IBOutlet UIButton *downloadButton;
@property (weak, nonatomic) IBOutlet UIButton *uploadButton;
@property (weak, nonatomic) IBOutlet UIButton *editButton;
@property (weak, nonatomic) IBOutlet UIButton *viewButton;
@property (weak, nonatomic) IBOutlet UILabel *errorLabel;

@end

@implementation DLMainViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    NSString *savedIP = [[NSUserDefaults standardUserDefaults]objectForKey:kIpKey];
    if (savedIP) {
        [self attemptConnectionWithIP:savedIP];
    }

    

    _fileManager = [[DLFileManager alloc]init];

    _tableView.dataSource = self;
    _tableView.delegate = self;
    _tableView.tableFooterView = [[UIView alloc]initWithFrame:CGRectZero];
    _tableView.allowsSelection = YES;

    self.view.backgroundColor = [UIColor colorWithWhite:0.950 alpha:1.000];
    _downloadButton.enabled = _uploadButton.enabled = NO;
    self.edgesForExtendedLayout = UIRectEdgeNone;
    
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc]initWithTarget:self
                                                                         action:@selector(endEditing)];
    UIView *tapView = [[UIView alloc]initWithFrame:CGRectMake(0,
                                                             0,
                                                             self.view.frame.size.width,
                                                              self.view.frame.size.height - _tableView.frame.size.height)];
    tapView.backgroundColor = self.view.backgroundColor;
    [tapView addGestureRecognizer:tap];
    [self.view insertSubview:tapView atIndex:0];

    
    [self customizeButton:_serverIPButton];
    [self customizeButton:_uploadButton];
    [self customizeButton:_downloadButton];
    [self customizeButton:_editButton];
    [self customizeButton:_viewButton];

    _errorLabel.textColor = [UIColor colorWithRed:0.071 green:0.544 blue:0.152 alpha:1.000];

}


- (void)customizeButton:(UIButton *)button{
    button.layer.borderWidth = 1.0;
    button.layer.borderColor =[UIColor colorWithRed:0.000 green:0.792 blue:0.271 alpha:1.000].CGColor;
    button.layer.cornerRadius = 4.0;
    button.layer.masksToBounds = YES;
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    
    [self.navigationController setNavigationBarHidden:YES animated:NO];

   
    [_fileManager loadFiles];
    [self.tableView reloadData];
    
}

- (void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    [self.navigationController setNavigationBarHidden:NO animated:NO];
    if ([DLNetworkingBridge isConnectd]) {
        [[NSUserDefaults standardUserDefaults]setObject:_serverIPButton.titleLabel.text forKey:kIpKey];
    }
}


#pragma mark - Button Actions

- (IBAction)setTestingOption:(UISegmentedControl *)sender {
    setTestingOption((ErrorOption)sender.selectedSegmentIndex + 1);
}

- (IBAction)download:(UIButton *)sender {

    [DLNetworkingBridge downloadFileName:_fileTextField.text
                         completionBlock:^(BOOL success, NSData *fileData) {
                             
                             if (success) {
                                 
                                 [_fileManager addFileWithName:_fileTextField.text data:fileData];
                                 [self.tableView reloadData];
                             }
                             else{
                                 NSLog(@"Failed to download");
                             }
                             
                         }];
    
    
    
}
- (IBAction)editTapped:(UIButton *)sender {
    
    if ( [self.tableView isEditing]) {
        [self.tableView setEditing:NO animated:YES];
        [sender setTitle:@"Remove Items" forState:UIControlStateNormal];
        [sender setTitleColor:[[[UIApplication sharedApplication]keyWindow]tintColor] forState:UIControlStateNormal];


    }else {
        [self.tableView setEditing:YES animated:YES];
        [sender setTitle:@"End Editing" forState:UIControlStateNormal];
        [sender setTitleColor:[UIColor redColor] forState:UIControlStateNormal];

        
    }
    
   
}
- (IBAction)didTapViewFile:(UIButton *)sender {
    
    DLFileViewerViewController *fvVC = [[DLFileViewerViewController alloc]initWithFile:_selectedFile];
    [self.navigationController pushViewController:fvVC animated:YES];
    
    
}

- (IBAction)upload:(UIButton *)sender {
    
    [DLNetworkingBridge uploadData:_selectedFile.fileData
                          fileName:[_selectedFile fullName]
                   completionBlock:^(BOOL success) {
                       
                       if (success) NSLog(@"Uploaded file: %@", _selectedFile.fileName);
                       else NSLog(@"Upload failed");
                       
                   }];
    
}
- (IBAction)enterNewIP:(id)sender {
    [self promptForIP];
}
- (IBAction)errorPercentageChanged:(UISlider *)sender {
    [DLNetworkingBridge setErrorProbability:sender.value];
    _errorLabel.text = [NSString stringWithFormat:@"%d%%", (int)sender.value];

}

#pragma mark - Services

- (void)endEditing{
    [self.view endEditing:YES];
    
    if ([self.tableView isEditing]) {
        [self editTapped:_editButton];

    }
}
- (void)promptForIP{
    
    [UIAlertView showWithTitle:@"ECE 483"
                       message:@"Enter Server IP Address"
                         style:UIAlertViewStylePlainTextInput
             cancelButtonTitle:@"Cancel"
             otherButtonTitles:@[@"Connect"]
                      tapBlock:^(UIAlertView *alertView, NSInteger buttonIndex) {
                          
                          if (buttonIndex == [alertView cancelButtonIndex]) {
                              NSLog(@"Cancelled");
                              [self validateButtons];
                          }
                          else {
                              UITextField *ipTextField = [alertView textFieldAtIndex:0];
                              
#ifdef AUTO_FILL
                              ipTextField.text = @"192.168.1.22";
#endif
                              [self attemptConnectionWithIP:ipTextField.text];
                          }
                          
                          
                      }];
}

- (void)attemptConnectionWithIP:(NSString *)ipStirng{
    
    [DLNetworkingBridge connectWithIPString:ipStirng
                               completionBlock:^(BOOL success) {
                                   
                                   if (success) {
                                       [_serverIPButton setTitle:ipStirng forState:UIControlStateNormal];
                                       [[NSUserDefaults standardUserDefaults]setObject:ipStirng forKey:kIpKey];

                                   }
                                   else [self showConnectionError:ipStirng];
                                   
                                   [self validateButtons];
                                   
                                   
                               }];
    
}

- (void)showConnectionError:(NSString *)ipUsed{
    
    [UIAlertView showWithTitle:@"Error"
                       message:[NSString stringWithFormat:@"Could not connect with [%@]", ipUsed]
             cancelButtonTitle:@"Give Up"
             otherButtonTitles:@[@"Try Again"]
                      tapBlock:^(UIAlertView *alertView, NSInteger buttonIndex) {
                          
                          if (buttonIndex == [alertView cancelButtonIndex]) {
                              NSLog(@"Gave up");
                          }
                          else {
                              [self promptForIP];
                          }
                      }];
}
- (void)validateButtons{
    
    if ([DLNetworkingBridge isConnectd]) {
        
        if (!_fileTextField.text || [_fileTextField.text isEqualToString:@""]) {
            _downloadButton.enabled = NO;
        }else {
            _downloadButton.enabled = YES;
            _uploadButton.enabled = ([_fileManager fileWithName:_fileTextField.text] != nil);
        }
    }else _downloadButton.enabled = _uploadButton.enabled = NO;
}
#pragma mark - UITextField Delegate
- (void)textFieldDidEndEditing:(UITextField *)textField{
    [self validateButtons];
    
    DLFile *file = [_fileManager fileWithName:textField.text];
    if (file) {
        NSInteger index = [_fileManager indexOfFile:file];
        NSIndexPath *indexPath = [NSIndexPath indexPathForRow:index inSection:0];
        [self tableView:self.tableView didSelectRowAtIndexPath:indexPath];
    }
    
    
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField{
	[self.view endEditing:YES];
    return YES;
}
- (void)textFieldDidBeginEditing:(UITextField *)textField{
    [self.tableView setEditing:NO animated:YES];
#ifdef AUTO_FILL
    _fileTextField.text = @"Test.m";
#endif
}


#pragma mark - UITbleViewDelegate / DataSource

- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath{
    return YES;
}


- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    // If row is deleted, remove it from the list.
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        [_fileManager deleteFileAtIndex:indexPath.row];
        [tableView deleteRowsAtIndexPaths:[NSArray arrayWithObject:indexPath] withRowAnimation:UITableViewRowAnimationFade];
    }
    
}



- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return [_fileManager numberOfFiles];
}

static const CGFloat kHeaderHeight = 25.0;
- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    return kHeaderHeight;
}
- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    if (section == 0) {
        
        UILabel *header =[[UILabel alloc]initWithFrame:CGRectMake(0,
                                                                  0,
                                                                  [[UIApplication sharedApplication]keyWindow].frame.size.width,
                                                                  kHeaderHeight)];
        header.backgroundColor = [UIColor colorWithRed:0.000 green:0.792 blue:0.271 alpha:1.000];
        header.textColor = [UIColor whiteColor];
        header.font = [UIFont boldSystemFontOfSize:14];
        header.text = @"  Local Files";
        return header;
    }
    return nil;
}



- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    [tableView deselectRowAtIndexPath:indexPath animated:NO];
    [tableView setEditing:NO animated:YES];
    
    // One file already selected
    if (_selectedFile) {
    
        // Update model
        [_selectedFile setIsSelected:NO];
        
        // Unselect old cell
        NSInteger index = [_fileManager indexOfFile:_selectedFile];
        NSIndexPath *oldIndexPath = [NSIndexPath indexPathForRow:index inSection:0];
        UITableViewCell *oldCell = [tableView cellForRowAtIndexPath:oldIndexPath];
        if (oldCell.accessoryType == UITableViewCellAccessoryCheckmark) {
            oldCell.accessoryType = UITableViewCellAccessoryNone;
        }
    }
    
    // select new cell
    UITableViewCell *newCell = [tableView cellForRowAtIndexPath:indexPath];
    if (newCell.accessoryType == UITableViewCellAccessoryNone) {
        newCell.accessoryType = UITableViewCellAccessoryCheckmark;
        _selectedFile = [_fileManager fileObjectAtIndex:indexPath.row];
        
        // Update model
        [_selectedFile setIsSelected:YES];
        
        _fileTextField.text = [_selectedFile fullName];
        [self validateButtons];
    }
 
    

}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"cell"];
    if (!cell) {
        cell = [[UITableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"cell"];
    }
    
    DLFile *fileObject = [_fileManager fileObjectAtIndex:indexPath.row];
    
    cell.textLabel.text = [fileObject fullName];
    cell.accessoryType = ([fileObject isSelected])? UITableViewCellAccessoryCheckmark: UITableViewCellAccessoryNone ;
    return cell;
}
@end
