//
//  DLNetworkingBridge.m
//  ECE 483-201
//
//  Created by Denis Lemos on 3/8/14.
//  Copyright (c) 2014 Denis Lemos. All rights reserved.
//

#import "DLNetworkingBridge.h"
#import "DLNetworking.h"
#import "SVProgressHUD.H"
#import "Utility.h"

static Connection client, server;
static char * kQueueName = "Networking Queue";
static dispatch_queue_t _backgroundQueue;

void uploadProgress(float currentProgress);
void downloadProgress(float currentProgress);


@interface DLNetworkingBridge ()
@property (atomic, assign) BOOL connected;
@property (atomic, strong) NSDate *startDate;
@end

@implementation DLNetworkingBridge

#pragma mark - Public

+ (BOOL)isConnectd{
    return [[self instance]connected];
}

+ (void)connectWithIPString:(NSString *)ipString completionBlock:(void (^)(BOOL success))completion{
    
    // close previously opned sockets
    [self closeConnections];

    // Initialize class (only once)
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        [self initializeClass];
    });
    
    // Create Datagram socket
    if ((client.socket = socket(AF_INET, SOCK_DGRAM, IPPROTO_UDP))==-1){
        printf("\nERROR:socket()");
        completion(NO);
        return;
    }

    
    // Create server address info
    bzero(&server.address, sizeof(server.address));
    server.address.sin_family = AF_INET;
    server.address.sin_port = htons(SERVER_PORT);
    server.address.sin_addr.s_addr = inet_addr([ipString UTF8String]);
    
    
    printf("\nServer Address[%s] Port[%d]",
           [ipString UTF8String], ntohs(server.address.sin_port));

    // In Main queue
    [SVProgressHUD showWithMaskType:SVProgressHUDMaskTypeBlack];
    
    dispatch_async(_backgroundQueue, ^{
        
        // In Background queue
        BOOL success = checkConnection(&client, &server); // Blocks
        
        // Get main queue to update UI and call completion block
        dispatch_async(dispatch_get_main_queue(), ^{
            
            [self instance].connected = success;
            // Update Spinner
            if (success)[SVProgressHUD showSuccessWithStatus:@"Connected"];
            else [SVProgressHUD showErrorWithStatus:@"Failed to Connect"];
            
            // Completion
            completion(success);
            
        });
    });
}


+ (void)downloadFileName:(NSString *)name
         completionBlock:(void (^)(BOOL success, NSData *fileData))completion{
    

    // In Main queue
    downloadProgress(0.2);
    
    DLNetworkingBridge *singleton = [self instance];
    singleton.startDate = [NSDate date];
    
    
    // -------------------- Perform Network operation in background queue
    dispatch_async(_backgroundQueue, ^{
        
        // Build command string
        NSString *commandString = [NSString stringWithFormat:@"%s %@",COMMAND_DOWNLOAD, name];
        
        // ---------------In backround queue--------------
        

        if (rdt_send(&client, &server, (Byte *)[commandString UTF8String], [commandString length]+1)) {

            uint8_t *dataBuffer = NULL;
            size_t dataSize;
            
            receiveProgress = downloadProgress;
            BOOL success = rdt_receive(&client,
                                       &server,
                                       (Byte **)&dataBuffer,
                                       &dataSize);
            
            
            NSData *fileData = nil;
            if (dataSize > 0 && success) {
                // Create NSData by copying buffer
                fileData = [NSData dataWithBytes:dataBuffer length:dataSize];

                free(dataBuffer);
          
            }else success = FALSE;
            
            // Get main queue to update UI and call completion block
            dispatch_async(dispatch_get_main_queue(), ^{
                
                // ---------------In Main queue--------------
                
                DLNetworkingBridge *singleton = [self instance];
                NSTimeInterval timeElapsed = [[NSDate date] timeIntervalSinceDate:singleton.startDate];
                NSLog(@"Download Time %f", timeElapsed);
    
                
                // Update Spinner
                if (success)[SVProgressHUD showSuccessWithStatus:@"Success"];
                else [SVProgressHUD showErrorWithStatus:@"Failed to Download File"];
                
                completion(success, fileData);
                
            });
            
            // ---------------In backround queue--------------
            
        }
    });
    
}

+ (void)uploadData:(NSData *)data
          fileName:(NSString *)name
   completionBlock:(void (^)(BOOL success))completion{
    
    // In Main queue
    uploadProgress(0.2);
    
     // -------------------- Perform Network operation in background queue
    dispatch_async(_backgroundQueue, ^{
        
        // Build command string
        NSString *commandString = [NSString stringWithFormat:@"%s %@",COMMAND_UPLOAD, name];
        
        // ---------------In backround queue--------------
        
        if (rdt_send(&client,
                     &server,
                     (Byte *)[commandString UTF8String],
                     [commandString length]+1)) {
   
            
            // Make Data Buffer
            void *dataBuffer = calloc([data length], sizeof(uint8_t));
            [data getBytes:dataBuffer];
            
            
            sendProgress = uploadProgress;
            
            BOOL success = rdt_send(&client,
                                    &server,
                                    (Byte *)dataBuffer,
                                    [data length]);
            
            
            // Get main queue to update UI and call completion block
            dispatch_async(dispatch_get_main_queue(), ^{

                // ---------------In Main queue--------------
                
                // Update Spinner
                if (success)[SVProgressHUD showSuccessWithStatus:@"Success"];
                
                else {
                    [SVProgressHUD showErrorWithStatus:@"Failed to Upload File"];
                }
                
                completion(success);

            });
            
            // ---------------In backround queue--------------
            
            free(dataBuffer);
            
        }
    });

    
}

+ (void)setErrorProbability:(CGFloat)percentage{
    setErrorProbability(percentage);
}


#pragma mark - Private
void uploadProgress(float currentProgress){
    dispatch_async(dispatch_get_main_queue(), ^{
        
        // ---------------In Main queue--------------
        [SVProgressHUD showProgress:currentProgress
                             status:@"Uploading"
                           maskType:SVProgressHUDMaskTypeBlack];
        
    });
 
}
void downloadProgress(float currentProgress){
    dispatch_async(dispatch_get_main_queue(), ^{
        
        // ---------------In Main queue--------------
        [SVProgressHUD showProgress:currentProgress
                             status:@"Downloading"
                           maskType:SVProgressHUDMaskTypeBlack];
        
    });
    
}
+ (DLNetworkingBridge *)instance{
    
    static DLNetworkingBridge *_instance;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        _instance = [[DLNetworkingBridge alloc]init];
    });
    return _instance;
}

+ (void)initializeClass{
    [[NSNotificationCenter defaultCenter]addObserver:self
                                            selector:@selector(appWillTerminate)
                                                name:UIApplicationWillTerminateNotification
                                              object:nil];
    // Initialize background queue here too
    _backgroundQueue = dispatch_queue_create(kQueueName, DISPATCH_QUEUE_SERIAL);
    
    [[SVProgressHUD appearance]setHudFont:[UIFont fontWithName:@"HelveticaNeue-Light" size:16]];
}

+ (void)appWillTerminate{
    [[NSNotificationCenter defaultCenter]removeObserver:self];
    [self closeConnections];
}

+ (void)closeConnections{
    
    [self instance].connected = NO;
    close(client.socket);
    close(server.socket);
}
@end
