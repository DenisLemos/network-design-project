//
//  File.m
//  ECE 483-201
//
//  Created by Denis Lemos on 3/6/14.
//  Copyright (c) 2014 Denis Lemos. All rights reserved.
//

#import "DLFile.h"


@implementation DLFile
@synthesize isSelected =_selected;
@dynamic fileData;
@dynamic extension;
@dynamic timestamp;
@dynamic fileName;

+ (NSString *)entityName{
    return @"DLFile";
}
- (void)awakeFromFetch{
    self.isSelected = NO;
}


- (NSString *)fullName{
    return [NSString stringWithFormat:@"%@%@%@", self.fileName, @".", self.extension];
}



@end
