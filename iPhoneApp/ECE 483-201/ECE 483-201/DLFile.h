//
//  File.h
//  ECE 483-201
//
//  Created by Denis Lemos on 3/6/14.
//  Copyright (c) 2014 Denis Lemos. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>


@interface DLFile : NSManagedObject

@property (nonatomic, assign) BOOL isSelected;

@property (nonatomic, retain) NSData * fileData;
@property (nonatomic, retain) NSString * extension;
@property (nonatomic, retain) NSDate * timestamp;
@property (nonatomic, strong) NSString * fileName;
+ (NSString *)entityName;
- (NSString *)fullName;


@end
