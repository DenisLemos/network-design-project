#!/bin/sh

echo "Closing XCode..."
for KILLPID in `ps ax | grep 'Xcode.app' | awk ' { print $1;}'`; do 
kill -9 $KILLPID;
done


echo "Cleaning Build..."
xcodebuild clean


echo "Deleting Derived Data..."
for DERDATADIR in `xcodebuild -showBuildSettings | grep -m 1 "CONFIGURATION_BUILD_DIR" | grep -oEi "\/.*" | awk '{split($0,a,"/Build/Products"); print a[1]}'`; do
rm -rf $DERDATADIR;
done


echo "Resetting Simulator If Open..."
osascript makeitwork_resetsimulator.applescript


echo "Reinstalling Pods..."
rm -rf Pods/ Podfile.lock *.xcworkspace
pod install


echo "Opening XCode..."
open *.xcworkspace
