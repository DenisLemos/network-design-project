
// To check if a library is compiled with CocoaPods you
// can use the `COCOAPODS` macro definition which is
// defined in the xcconfigs so it is available in
// headers also when they are imported in the client
// project.


// SVProgressHUD
#define COCOAPODS_POD_AVAILABLE_SVProgressHUD
#define COCOAPODS_VERSION_MAJOR_SVProgressHUD 1
#define COCOAPODS_VERSION_MINOR_SVProgressHUD 0
#define COCOAPODS_VERSION_PATCH_SVProgressHUD 0

// UIAlertView+Blocks
#define COCOAPODS_POD_AVAILABLE_UIAlertView_Blocks
#define COCOAPODS_VERSION_MAJOR_UIAlertView_Blocks 0
#define COCOAPODS_VERSION_MINOR_UIAlertView_Blocks 8
#define COCOAPODS_VERSION_PATCH_UIAlertView_Blocks 1

